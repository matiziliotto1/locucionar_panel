<?php
    require_once "conexionDB.php";

    class ControladorUsuarios{

        //SOLO ACEPTA VALORES ALFANUMERICOS, NO ACEPTA MAILS
        function sanitizar($frase){
            $paso=false;
            $expresion="/^[a-zA-ZñÑ0-9]+$/";
            $paso=preg_match($expresion,$frase);
            return $paso;
        }

        function autenticar($usuario,$password){
            $autentico=false;
            if(self::sanitizar($usuario) and self::sanitizar($password)){
                //buscar usuario en la base de datos a ver si esta
                $conectar=new ConexionDB();
                $conexion=$conectar->inicializar();
                $sql="SELECT * FROM usuarios_panel WHERE usuario='$usuario'";
                $consulta=$conexion->query($sql) or die("Error al consultar");
                $nFilas=mysqli_num_rows($consulta);
                $consulta=mysqli_fetch_array($consulta);

                //Si no existe el usuario ingresado
                if($nFilas==0){
                    return $autentico;
                }
                if($nFilas==1 && password_verify($password, $consulta['password'])){
                    $autentico=true;
                }
            }
            return $autentico;
        }

        function logout(){
            session_unset();
            session_destroy();
            header("Location: login.php");
        }

        function getUsuarios(){
            $usuarios=array();
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();
            $sql="SELECT * FROM usuarios_panel";
            $consulta=$conexion->query($sql) or die ("Error al buscar los usuarios");
            while($row = mysqli_fetch_array($consulta)) {
                $usuarios[]=$row;
            }
            return $usuarios;
        }

        function getUsuario($id=""){
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();
            $sql="SELECT * FROM usuarios_panel WHERE id_usuario=$id";
            $consulta=$conexion->query($sql) or die("Error al buscar el usuario");
            while($row = mysqli_fetch_assoc($consulta)) {
                $usuarios[]=$row;
            }
            return $usuarios[0];
        }

        //ACA TAMBIEN TENGO QUE SANITIZAR LAS ENTRADAS, SINO ME PUEDEN PONER CUALQUIER CODIGO SQL COMO NOMBRE U OTRO ATRIBUTO

        //que mandar en caso de errores, para no matar la pagina
        function modificarUser(array $request){
            $error=array();
            $idUser=$request['id'];
            if(self::sanitizar($request['nombre']) and self::sanitizar($request['apellido']) and self::sanitizar($request['userName'])){
                //paso los controles
                try {
                    isset($request['administrador']) ? $request['administrador'] = 1 : $request['administrador'] = 0;
                    $conectar=new ConexionDB();
                    $conexion=$conectar->inicializar();
                    $sql="UPDATE `usuarios_panel` SET `usuario`='$request[userName]',`nombre`='$request[nombre]',`apellido`='$request[apellido]',`administrador`=$request[administrador] WHERE id_usuario=$idUser";
                    $consulta=$conexion->query($sql);
                    if(! $consulta){
                        throw new Exception("Error modificando usuario", 1);
                    }
                    $error['valor']=true;
                    $error['mensaje']="Usuario modificado correctamente";
                } catch (Exception $e) {
                    $error['valor']=false;
                    $error['mensaje']="Error al modificar el usuario";
                }
            }else{
                $error['valor']=false;
                $error['mensaje']="Datos incorrectos, solo se aceptan valores alfanumericos";
            }
            return $error;
        }

        function agregarUsuario(array $request){
            if(self::sanitizar($request['nombre']) and self::sanitizar($request['apellido']) and self::sanitizar($request['userName']) and self::sanitizar($request['password'])){
                //paso los controles
                try{
                    isset($request['administrador']) ? $request['administrador'] = 1 : $request['administrador'] = 0;
                    $conectar=new ConexionDB();
                    $conexion=$conectar->inicializar();

                    $hashed_password = password_hash($request['password'], PASSWORD_DEFAULT);

                    $sql="INSERT INTO `usuarios_panel`(`id_usuario`, `usuario`, `password`, `nombre`, `apellido`, `administrador`) VALUES (null,'$request[userName]','$hashed_password','$request[nombre]','$request[apellido]',$request[administrador])";
                    $consulta=$conexion->query($sql);

                    if(! $consulta){
                        throw new Exception("Error agregando usuario", 1);
                    }
                    
                    $error['valor']=true;
                    $error['mensaje']="Usuario agregado correctamente";
                }catch(Exception $e){
                    $error['valor']=false;
                    $error['mensaje']="Error al agregar el usuario";
                }
            }else{
                $error['valor']=false;
                $error['mensaje']="Datos incorrectos, solo se aceptan valores alfanumericos";
            }
            return $error;
        }

        function eliminarUsuario(array $request){
            $error=array();
            try {
                $id=$request['id'];
                $conectar=new ConexionDB();
                $conexion=$conectar->inicializar();
                $sql="DELETE FROM `usuarios_panel` WHERE id_usuario=$id";
                $consulta=$conexion->query($sql);
                if(! $consulta){
                    throw new Exception("Error eliminando usuario", 1);
                }
                $error['valor']=true;
                $error['mensaje']='Usuario eliminado correctamente';
            } catch (Exception $e) {
                $error['valor']=false;
                $error['mensaje']='Error al eliminar el usuario';              
            }
            return $error;
        }

        function isAdmin($usuario)
        {
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();
            $sql="SELECT * FROM `usuarios_panel` WHERE usuario='$usuario'";
            $consulta=$conexion->query($sql) or die ("Error al chequear si el usuario es admin");
            $consulta=mysqli_fetch_array($consulta);
            return $consulta['administrador'];
        }

        function userExists($username="")
        {
            $error=array();
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();
            $sql="SELECT usuario FROM `usuarios_panel` WHERE usuario='$username'";
            $consulta=$conexion->query($sql) or die ("Error al chequear si el usuario existe");
            
            $nFilas=mysqli_num_rows($consulta);
            if($nFilas == 1 ){
                $error['error']="El usuario ingresado ya existe, por favor ingrese uno nuevo";
                return $error;
            }
            $error['error']="";
            return $error;
        }
    }
?>
