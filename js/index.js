function eliminarError(){
    document.getElementById('error_username').remove();
}

$(document).ready(function () {
    $('#userNameModal').on('keyup' ,function (event){
        var username = $('#userNameModal').val();
        $.ajax({
            url: "llamadasAjax.php",
            type: 'get',
            data: { "opcion": "userExists", "username": username },
            dataType: "json",
            success: function (response) {
                if(response['error']==""){
                    $('#error_username').remove();
                }
                else{
                    $('#userNameModal').after('<strong id="error_username" style="color:#FF2626;">'+response['error']+'</strong>')
                }
                
            }
        });
    });
    
    //PRIMERO TENGO QUE BUSCAR TODOS LOS USUARIOS PARA LISTARLOS
    //TENGO QUE BUSCAR TODAS LAS NOTIFICACIONES GUARDADAS

    //FUNCION ELIMINAR USUARIO, MODIFICAR Y AGREGAR
    //---------------parte de los usuarios--------------------//

    $('#modalFormulario').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var modal = $(this);
        var idButton = $(button).attr('id');


        if (idButton == 'modificarUsuarioButton') {
            modal.find('.modal-title').text('Modificar')
            var idUsuario = $(button).val()


        
            $('#divPasswordModal').hide();

            $.ajax({
                type: "get",
                url: "llamadasAjax.php",
                data: { "opcion": "getUsuario", "id": idUsuario },
                dataType: "json",
                success: function (response) {
                    //ACA LE VOY ASIGNANDO LOS VALORES AL MODAL
                    $('#nombreModal').val(response['nombre']);
                    $('#apellidoModal').val(response['apellido']);
                    $('#userNameModal').val(response['usuario']);
                    if (response['administrador'] == 1) {
                        $('#administradorModal').prop('checked', true);
                    }
                }
            });
            $('#botonModal').html("" +
                "<button type='submit' class='btn btn-primary' formaction=usuarios.php?opcion=modificarUser&id=" + idUsuario + ">Modificar</button>"
            );

        } else {
            if (idButton == 'agregarUsuarioButton') {
                modal.find('.modal-title').text('Agregar Usuario');

                $('#divPasswordModal').show();

                $('#nombreModal').val("");
                $('#apellidoModal').val("");
                $('#userNameModal').val("");
                $('#administradorModal').prop('checked', false);
                $('#botonModal').html("" +
                    "<button type='submit' class='btn btn-primary' formaction='usuarios.php?opcion=agregarUser'>Agregar</button>"
                );
            }
        }
    });

    $('#modalEliminar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idUsuario = $(button).val()
        $.ajax({
            type: "get",
            url: "llamadasAjax.php",
            data: { "opcion": "getUsuario", "id": idUsuario },
            dataType: "json",
            success: function (response) {
                $('#parrafoEliminar').html(""+
                    "<b>Usuario: </b>"+response['usuario']
                )
            }
        });

        $('#botonEliminar').html(""+
        "<a class='btn btn-primary' href=usuarios.php?opcion=eliminarUser&id="+idUsuario+">Eliminar</a>"
        )
    });


    $('#modalEliminarNotificacion').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idNotificacion = $(button).val()
        $.ajax({
            type: "get",
            url: "llamadasAjax.php",
            data: { "opcion": "getNotificacion", "id": idNotificacion },
            dataType: "json",
            success: function (response) {
                $('#parrafoEliminarNotificacion').html(""+
                    "<b>Titulo: </b>"+response['titulo']
                )
            }
        });

        $('#botonEliminarNotificacion').html(""+
        "<a class='btn btn-primary' href=index.php?opcion=eliminarNotificacion&id="+idNotificacion+">Eliminar</a>"
        )
    });

    //---------------parte de las notificaciones--------------------//


    //FUNCION ELIMINAR NOTIFICACION, CREAR, ENVIAR Y REENVIAR
});