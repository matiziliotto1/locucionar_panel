<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- jQuery -->
    <title>Pago mediante MercadoPago</title>

    <style>
        #div_carga{
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
            z-index:1;
            opacity: .8;
            background-color: #F5F5F5;
        }
        #loader{
            position:absolute;
            top:50%;
            left: 50%;
            margin-top: -25px;
            margin-left: -25px;
        }
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
</head>

<div class='response col-xl-6 mx-auto pl-0 pr-0'>
  <div class="card m-1 pt-3 pl-3 pr-3">
    <img class="card-img-top img-fluid" src="imagenes/MercadoPago-logoMano.png" alt="Logo">
    <hr>
    <div class="card-body pb-0">
      <?php 
        if(isset($_GET['id_cliente']) && $_GET['id_cliente'] != null && $_POST['id_cliente'] != "null"){
      ?>
      <h5 class="card-title">Ingrese el monto</h5>
      <form action="checkout.php" method="POST" id="form">
        <input type="hidden" id="id_cliente" name="id_cliente" value="<?php echo $_GET['id_cliente']; ?>">
        <div class="row justify-content-md-center mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">$</div>
            </div>
            <input class="form-control" type="number" name="monto" id="monto" step="0.01" min="0" placeholder="0.01" required>
          </div>
        </div>
        <div class="text-center">
          <input type="submit" value="Siguiente" id="siguiente" class="btn btn-primary mt-3" style="background-color: #009ee3;border:none">
        </div>
      </form>
      <?php
        }
        else{
      ?>
          <h5 class="card-title text-center text-danger">Ups, ocurrio un error!</h5>
          <h5 class="card-title text-center">Intente nuevamente saliendo de la aplicacion. Primero cierre sesión y luego vuelva a ingresar.</h5>          
      <?php
        }
      ?>
    </div>
  </div>
</div>
<div id="div_carga">
  <div class="spinner-border" role="status" style="color:#009ee3;" id="loader">
    <span class="sr-only">Loading...</span>
  </div>
</div>

<script type='text/javascript'>

  $(document).ready(function(){
    //Loader de la pagina
    $(window).load(function(){ 
      $('#div_carga').fadeOut();
    });

    //Loader al pasar a la pagina de pago
    $("#form").submit(function(e){
      var monto = $('#monto').val();
      var id_cliente = $('#id_cliente').val();

      $.ajax({
      url: 'checkout.php',
      type: 'post',
      data: {
        monto:monto,
        id_cliente:id_cliente
      },
      beforeSend: function(){
        // Show image container
        $('#div_carga').fadeIn();
      },
      success: function(response){
        $('.response').empty();
        $('.response').append(response);
      },
      complete:function(data){
        // Hide image container
        $('#div_carga').fadeOut();
      }
      });
    });
  });
</script>