<?php 
  if(!(isset($_POST['id_cliente']) && $_POST['id_cliente'] != null && $_POST['id_cliente'] != "null")){
    header('Location: error.php');
  }
?>

<?php
// SDK de Mercado Pago
require __DIR__ .  '/vendor/autoload.php';

// Agrega credenciales
//TEST-894658496562814-050604-6f365ca222837e6ebb98d5bd0f35d15f-562339626
//MercadoPago\SDK::setAccessToken('8640123190343809,AEHnvLOGGqad3K9F1Rjh4zZoIq6l5tHL');
MercadoPago\SDK::setClientId("8640123190343809");
MercadoPago\SDK::setClientSecret("AEHnvLOGGqad3K9F1Rjh4zZoIq6l5tHL");

// Crea un objeto de preferencia
$preference = new MercadoPago\Preference();

// Crea un ítem en la preferencia
$item = new MercadoPago\Item();
$item->title = 'PagoApp';
$item->quantity = 1;
$item->unit_price = $_POST['monto']/1;
$preference->external_reference = "sistema".$_POST['id_cliente'];
$preference->items = array($item);
$preference->payment_methods = array(
  "excluded_payment_types" => array(
    array("id" => "ticket"),
    array("id" => "atm"),
    array("id" => "bank_transfer")
  ),
);
$preference->back_urls = ['success' => "https://clientes.locucionar.com/app_clientes/MercadoPago/transaccionExitosa.php", 'pending' => "https://clientes.locucionar.com/app_clientes/MercadoPago/transaccionPendiente.php", "failure" => "https://clientes.locucionar.com/app_clientes/MercadoPago/transaccionFallida.php"];
$preference->save();
?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- jQuery -->
	<title>Pago mediante MercadoPago</title>
</head>

<div class='response col-xl-6 mx-auto pl-0 pr-0'>
  <div class="card m-1 pt-3 pl-3 pr-3">
    <img class="card-img-top img-fluid" src="imagenes/MercadoPago-logoMano.png" alt="Logo">
    <hr>
    <div class="card-body pb-0">
      <h5 class="card-title">El monto ingresado es: $<?php echo $item->unit_price ?></h5>
      <p class="card-text">Si todo es correcto, haga click en pagar para continuar.</p>
        <div class="text-center mt-3">
          
          <!-- Boton de MercadoPago -->
          <form action="/procesar-pago" method="POST">
            <script src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js" data-preference-id="<?php echo $preference->id; ?>">
            </script>
          </form>

        </div>
    </div>
  </div>
</div>