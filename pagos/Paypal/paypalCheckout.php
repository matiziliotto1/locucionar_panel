<?php
session_start();
require_once('config.php');
//ACA SE VA A VER UN DETALLE DE LA COMPRA Y EL BOTON DE PAYPAL
$monto;
$idCliente;
if (isset($_POST['monto']) and isset($_SESSION['id_cliente'])) {
	$monto = $_POST['monto'];
	$idCliente = $_SESSION['id_cliente'];
} else {
	//tengo que redirigir a otro lado
	header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- jQuery -->
	<title>Pago mediante Paypal</title>
</head>

<body>
	<script src="https://www.paypal.com/sdk/js?client-id=<?php echo (PayPalClientId) ?>"></script>

	<div class='response col-xl-6 mx-auto pl-0 pr-0'>
		<div class="card m-1 pt-3 pl-3 pr-3">
			<img class="card-img-top img-fluid" src="imagenes/PayPal-logo.png" alt="Logo">
			<hr>
			<div class="card-body pb-0">
				<h5 class="card-title">El monto ingresado es: USD <?php echo $monto ?></h5>
				<p class="card-text">Si todo es correcto, haga click en pagar para continuar.</p>

				<div class="text-center mt-3" id="paypalButton">

				</div>
			</div>
		</div>
	</div>
</body>

<script>
	//mas parametros en order create
	paypal.Buttons({
		createOrder: function(data, actions) {
			// This function sets up the details of the transaction, including the amount and line item details.
			return actions.order.create({


				purchase_units: [{
					amount: {
						currency_code: "USD",
						value: "<?php echo $monto ?>",
						breakdown: {
							item_total: {
								currency_code: "USD",
								value: "<?php echo $monto ?>"
							},
						}
					},
					items: [{
						name: "PagoApp",
						sku: "<?php echo ('sistema' . $idCliente); ?>",
						quantity: "1",
						unit_amount: {
							currency_code: "USD",
							value: "<?php echo $monto ?>"
						}
					}]
				}],
			});
		},
		onApprove: function(data, actions) {
			// This function captures the funds from the transaction.
			return actions.order.capture().then(function(details) {
				// This function shows a transaction success message to your buyer.

				window.location.href = "transaccionExitosa.php";

			});
		},
		onError: function(err) {
			// Show an error page here, when an error occurs
			window.location.href = "transaccionFallida.php";
		}
	}).render('#paypalButton');
</script>

</html>