<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- jQuery -->
    <title>Error MercadoPago</title>
</head>

<div class='response col-xl-6 mx-auto pl-0 pr-0'>
    <div class="card m-1 pt-3 pl-3 pr-3">
        <img class="card-img-top img-fluid" src="imagenes/PayPal-logo.png" alt="Logo">
        <hr>
        <div class="card-body pb-0">
            <h5 class="card-title text-center text-danger">Ups, ocurrio un error!</h5>
            <h5 class="card-title text-center">Intente nuevamente saliendo de la aplicacion. Primero cierre sesión y luego vuelva a ingresar.</h5>
        </div>
    </div>
</div>