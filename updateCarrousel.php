<?php
	require_once "conexionDB.php";
?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	    
		$new_imagen1 = $_FILES['imagen0']['name'];
		$new_imagen2 = $_FILES['imagen1']['name'];
		$new_imagen3 = $_FILES['imagen2']['name'];
		$new_imagen4 = $_FILES['imagen3']['name'];
		$new_imagen5 = $_FILES['imagen4']['name'];

		$new_titulo_imagen1 = $_POST['titulo_imagen0'];
	    $new_titulo_imagen2 = $_POST['titulo_imagen1'];
	    $new_titulo_imagen3 = $_POST['titulo_imagen2'];
	    $new_titulo_imagen4 = $_POST['titulo_imagen3'];
	    $new_titulo_imagen5 = $_POST['titulo_imagen4'];

	    $new_texto_imagen1 = $_POST['texto_imagen0'];
	    $new_texto_imagen2 = $_POST['texto_imagen1'];
	    $new_texto_imagen3 = $_POST['texto_imagen2'];
	    $new_texto_imagen4 = $_POST['texto_imagen3'];
	    $new_texto_imagen5 = $_POST['texto_imagen4'];

	    $new_orden1 = $_POST['orden0'];
	    $new_orden2 = $_POST['orden1'];
	    $new_orden3 = $_POST['orden2'];
	    $new_orden4 = $_POST['orden3'];
	    $new_orden5 = $_POST['orden4'];

	    if (isset($_POST['mostrar0'])) {
	    	$new_mostrar1=1;
	    }
	    else{
	    	$new_mostrar1=0;
	    }

	    if (isset($_POST['mostrar1'])) {
		    $new_mostrar2=1;
		}
		else{
			$new_mostrar2=0;
		}

		if (isset($_POST['mostrar2'])) {
	    	$new_mostrar3=1;
	    }
	    else{
	    	$new_mostrar3=0;
	    }

		if (isset($_POST['mostrar3'])) {
	    	$new_mostrar4=1;
	    }
	    else{
	    	$new_mostrar4=0;
	    }

		if (isset($_POST['mostrar4'])) {
	    	$new_mostrar5=1;
	    }
	    else{
	    	$new_mostrar5=0;
	    }


	    ini_set('upload-max-filesize', '10M');
	    ini_set('post_max_size', '10M');

	    $instruccion1 = "";
	    $instruccion2 = "";
	    $instruccion3 = "";
	    $instruccion4 = "";
	    $instruccion5 = "";

	    if ($new_imagen1) {
	        subirFoto(0);
	        $instruccion1 = "update carrousel_app set imagen='".$new_imagen1."', titulo_imagen='".$new_titulo_imagen1."', texto_imagen='".$new_texto_imagen1."',orden=".$new_orden1.",mostrar='".$new_mostrar1."' where id_imagen=1;";
	    } else {
	        $instruccion1 = "update carrousel_app set titulo_imagen='".$new_titulo_imagen1."', texto_imagen='".$new_texto_imagen1."',orden=".$new_orden1.",mostrar='".$new_mostrar1."' where id_imagen=1;";
	    }

	    if ($new_imagen2) {
	        subirFoto(1);
	        $instruccion2 = "update carrousel_app set imagen='".$new_imagen2."',titulo_imagen='".$new_titulo_imagen2."', texto_imagen='".$new_texto_imagen2."',orden=".$new_orden2.",mostrar='".$new_mostrar2."' where id_imagen=2;";
	    } else {
	        $instruccion2 = "update carrousel_app set titulo_imagen='".$new_titulo_imagen2."', texto_imagen='".$new_texto_imagen2."',orden=".$new_orden2.",mostrar='".$new_mostrar2."' where id_imagen=2;";
	    }

	    if ($new_imagen3) {
	        subirFoto(2);
	        $instruccion3 = "update carrousel_app set imagen='".$new_imagen3."',titulo_imagen='".$new_titulo_imagen3."', texto_imagen='".$new_texto_imagen3."',orden=".$new_orden3.",mostrar='".$new_mostrar3."' where id_imagen=3;";
	    } else {
	        $instruccion3 = "update carrousel_app set titulo_imagen='".$new_titulo_imagen3."', texto_imagen='".$new_texto_imagen3."',orden=".$new_orden3.",mostrar='".$new_mostrar3."' where id_imagen=3;";
	    }

	    if ($new_imagen4) {
	        subirFoto(3);
	        $instruccion4 = "update carrousel_app set imagen='".$new_imagen4."',titulo_imagen='".$new_titulo_imagen4."', texto_imagen='".$new_texto_imagen4."',orden=".$new_orden4.",mostrar='".$new_mostrar4."' where id_imagen=4;";
	    } else {
	        $instruccion4 = "update carrousel_app set titulo_imagen='".$new_titulo_imagen4."', texto_imagen='".$new_texto_imagen4."',orden=".$new_orden4.",mostrar='".$new_mostrar4."' where id_imagen=4;";
	    }

	    if ($new_imagen5) {
	        subirFoto(4);
	        $instruccion5 = "update carrousel_app set imagen='".$new_imagen5."',titulo_imagen='".$new_titulo_imagen5."', texto_imagen='".$new_texto_imagen5."',orden=".$new_orden5.",mostrar='".$new_mostrar5."' where id_imagen=5;";
	    } else {
	        $instruccion5 = "update carrousel_app set titulo_imagen='".$new_titulo_imagen5."', texto_imagen='".$new_texto_imagen5."',orden=".$new_orden5.",mostrar='".$new_mostrar5."' where id_imagen=5;";
	    }

	    $conectar=new ConexionDB();
    	$conexion=$conectar->inicializar();

	    $consulta1 = mysqli_query($conexion, $instruccion1) or die("Fallo en la consulta");
	    $consulta2 = mysqli_query($conexion, $instruccion2) or die("Fallo en la consulta");
	    $consulta3 = mysqli_query($conexion, $instruccion3) or die("Fallo en la consulta");
	    $consulta4 = mysqli_query($conexion, $instruccion4) or die("Fallo en la consulta");
	    $consulta5 = mysqli_query($conexion, $instruccion5) or die("Fallo en la consulta");
	    
	    mysqli_close($conexion);
	    header("location: configCarrousel.php?editado_correctamente=1");
	    //echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/crearNoticia.php?noticia_creada=1') </script>";
	}
?>

<?php
	function subirFoto($id){
		$message='';
	    if (isset($_POST['actualizarImagenes']) && $_POST['actualizarImagenes'] == 'Actualizar carrousel') {
	        // get details of the uploaded file
	        $fileTmpPath = $_FILES['imagen'.$id]['tmp_name'];
	        $fileName = $_FILES['imagen'.$id]['name'];
	        $fileNameCmps = explode(".", $fileName);
	        $fileExtension = strtolower(end($fileNameCmps));

	        // check if file has one of the following extensions
	        $allowedfileExtensions = array('jpg', 'png','jpeg');

	        if (in_array($fileExtension, $allowedfileExtensions)) {
	            // directory in which the uploaded file will be moved
	            $uploadFileDir = __DIR__.'/imagenes/';
	            $dest_path = $uploadFileDir . $fileName;
	            if (move_uploaded_file($fileTmpPath, $dest_path)) {
	                $message ='Imagen subida correctamente.';
	            } else {
	                $message = 'Hubo un problema subiendo la imagen al servidor.';
	            }
	        } else {
	            $message = 'Subida fallida. Tipos de imagen permitidos: ' . implode(',', $allowedfileExtensions);
	        }
	    }
	}
?>