-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-04-2020 a las 18:00:10
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `locucionar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrousel_app`
--

CREATE TABLE `carrousel_app` (
  `id_imagen` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `titulo_imagen` char(255) DEFAULT NULL,
  `texto_imagen` char(255) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `mostrar` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `carrousel_app`
--

INSERT INTO `carrousel_app` (`id_imagen`, `imagen`, `titulo_imagen`, `texto_imagen`, `orden`, `mostrar`) VALUES
(1, 'macri.jpg', 'hola1', '1232', 5, 1),
(2, 'fondo2.jpg', 'Hola4', '12323', 4, 1),
(3, 'fondo3.jpg', 'Hola3', '123', 3, 1),
(4, 'fondo5.jpg', 'Hola2', '12', 2, 1),
(5, 'fondo5.jpg', 'Hola1', '1', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones_app`
--

CREATE TABLE `notificaciones_app` (
  `id_notificacion` int(11) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `cuerpo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_creador` int(11) NOT NULL,
  `fecha_envio` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_panel`
--

CREATE TABLE `usuarios_panel` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `administrador` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `usuarios_panel`
--

INSERT INTO `usuarios_panel` (`id_usuario`, `usuario`, `password`, `nombre`, `apellido`, `administrador`) VALUES
(1, 'admin123', '$2y$10$HCmcqKeMPV0H18bg4AMKJOzUa9b1hQBCrcpho2gIg7PLoR4d9A5qa', 'Administrador', 'Administrador', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrousel_app`
--
ALTER TABLE `carrousel_app`
  ADD PRIMARY KEY (`id_imagen`);

--
-- Indices de la tabla `notificaciones_app`
--
ALTER TABLE `notificaciones_app`
  ADD PRIMARY KEY (`id_notificacion`),
  ADD KEY `creador` (`id_creador`);

--
-- Indices de la tabla `usuarios_panel`
--
ALTER TABLE `usuarios_panel`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrousel_app`
--
ALTER TABLE `carrousel_app`
  MODIFY `id_imagen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `notificaciones_app`
--
ALTER TABLE `notificaciones_app`
  MODIFY `id_notificacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios_panel`
--
ALTER TABLE `usuarios_panel`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notificaciones_app`
--
ALTER TABLE `notificaciones_app`
  ADD CONSTRAINT `creador` FOREIGN KEY (`id_creador`) REFERENCES `usuarios_panel` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
