-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 01-05-2020 a las 16:54:59
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `locucionar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tokens_x_usuarios`
--

CREATE TABLE `tokens_x_usuarios` (
  `id_usuario` int(11) NOT NULL,
  `id_token` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tokens_x_usuarios`
--
ALTER TABLE `tokens_x_usuarios`
  ADD PRIMARY KEY (`id_usuario`,`id_token`),
  ADD KEY `tokens-tokens_x_usuarios` (`id_token`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tokens_x_usuarios`
--
ALTER TABLE `tokens_x_usuarios`
  ADD CONSTRAINT `tokens-tokens_x_usuarios` FOREIGN KEY (`id_token`) REFERENCES `tokens` (`id_token`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `usu-tokens_x_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios_locucionar` (`id_usuario`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
