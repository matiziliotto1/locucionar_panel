-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-04-2020 a las 14:25:21
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `locucionar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrousel_app`
--

CREATE TABLE `carrousel_app` (
  `id_imagen` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `titulo_imagen` char(255) DEFAULT NULL,
  `texto_imagen` char(255) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `mostrar` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `carrousel_app`
--

INSERT INTO `carrousel_app` (`id_imagen`, `imagen`, `titulo_imagen`, `texto_imagen`, `orden`, `mostrar`) VALUES
(1, 'macri.jpg', 'hola1', '12232', 5, 1),
(2, 'fondo2.jpg', 'Hola4', '12323', 4, 1),
(3, 'fondo3.jpg', 'Hola3', '123', 3, 1),
(4, 'fondo5.jpg', 'Hola2', '12', 2, 1),
(5, 'fondo5.jpg', 'Hola1', '1', 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrousel_app`
--
ALTER TABLE `carrousel_app`
  ADD PRIMARY KEY (`id_imagen`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrousel_app`
--
ALTER TABLE `carrousel_app`
  MODIFY `id_imagen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
