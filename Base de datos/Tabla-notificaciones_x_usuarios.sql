-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 01-05-2020 a las 16:54:50
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `locucionar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones_x_usuarios`
--

CREATE TABLE `notificaciones_x_usuarios` (
  `id_notificacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `notificaciones_x_usuarios`
--
ALTER TABLE `notificaciones_x_usuarios`
  ADD PRIMARY KEY (`id_notificacion`,`id_usuario`),
  ADD KEY `usu-notificaciones_x_usuarios` (`id_usuario`),
  ADD KEY `id_notificacion` (`id_notificacion`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notificaciones_x_usuarios`
--
ALTER TABLE `notificaciones_x_usuarios`
  ADD CONSTRAINT `noti-notificaciones_x_usuarios` FOREIGN KEY (`id_notificacion`) REFERENCES `notificaciones_app` (`id_notificacion`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `usu-notificaciones_x_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios_locucionar` (`id_usuario`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
