<?php 
	session_start();
	if (!isset($_SESSION['usuarioValido'])) {
		header("Location: login.php");
	}
	include_once('header.php');
	require_once "conexionDB.php";
?>
<?php
	$conectar=new ConexionDB();
    $conexion=$conectar->inicializar();

    $instruccion = "select * from carrousel_app;";
    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

    $nfilas = mysqli_num_rows($consulta);
  	
    $imagenes = array();

  	for($i=0;$i<$nfilas && $i<5;$i++){
  		$fila = mysqli_fetch_array($consulta);
  		
  		array_push($imagenes, $fila);
  	}
?>


<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<style type="text/css">
	.card-custom{
		background-color: #435c70;
		color: white;
	}
</style>

		<div class="col-12 mx-auto pt-4">
		    <form method="POST" enctype="multipart/form-data" id="form" onsubmit="validarFormulario();"
		    action="updateCarrousel.php" >
		    	<div class="card-group">
		        	<?php
		        		for ($i=0;$i<$nfilas;$i++){
		        			echo "<div class='card m-1 card-custom'>
									<img src='imagenes/".$imagenes[$i]['imagen']."' class='card-img-top' alt='Error al cargar la imagen ".$imagenes[$i]['imagen']."' id='imgSalida".$i."' height='350'>
									<div class='card-body'>
						                <div class='custom-file mb-2'>
						                	<input type='file' id='imagen".$i."' name='imagen".$i."' onchange='checkextension(".$i.")' accept='.jpg,.jpeg,.png' style='display:none;'>
						                	<input type='button' class='btn btn-primary btn-block mx-auto' value='Seleccionar imagen' onclick="; echo '"clickInput('.$i.');"'; echo ">
						                </div>

										<h6 class='card-title pt-3'>Titulo de la imagen</h6>
										<input class='card-text form-control' name='titulo_imagen".$i."' value='".$imagenes[$i]['titulo_imagen']."'>

										<h6 class='card-title pt-3'>Texto de la imagen</h6>
										<textarea class='card-text form-control' name='texto_imagen".$i."' rows='3'>".$imagenes[$i]['texto_imagen']."</textarea>
										
										<h6 class='card-title pt-3'>Orden de visualizacion</h6>
											<select class='custom-select' name='orden".$i."' id='select".$i."'>";
											for($j=1;$j<6;$j++){
												if($imagenes[$i]['orden']==$j){
													echo "<option value='".$j."' selected>".$j."</option>";
												}
												else{
													echo "<option value='".$j."'>".$j."</option>";
												}
											};
									echo "
										</select>

										<h6 class='card-title pt-3'>Mostrar imagen?</h6>
										<input id='mostrar".$i."' type='checkbox' name='mostrar".$i."' "; if ($imagenes[$i]['mostrar']) {echo 'checked';} echo ">
									</div>
								</div>";
		        		}
		        	?>
		        </div>

				<?php
					if(isset($_REQUEST['editado_correctamente'])){
						if($_REQUEST['editado_correctamente'] == 1){
							echo "<div class='alert alert-success' role='alert' style='margin-top:3%;'>
									  Se actualizo correctamente el carrousel de la aplicacion
									  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
										<span aria-hidden='true'>&times;</span>
									  </button>
									</div>";
						}
					}
		        ?>

				<div id="div-alert-extension" class="alert alert-danger form-width" role="alert" style="margin-top:3%;display:none;">
					*Los formatos permitidos son jpeg, jpg y png.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div id="div-alert-selects" class="alert alert-danger form-width" role="alert" style="margin-top:3%;display:none;">
					Debe cambiar el orden de las imagenes, dos imagenes no pueden tener el mismo orden.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div id="div-alert-checkboxs" class="alert alert-danger form-width" role="alert" style="margin-top:3%;display:none;">
					Debe seleccionar al menos una imagen para mostrar.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

		        <div class="text-center">
		        	<input type="submit" value="Actualizar carrousel" id="btn-crear-noticia" name="actualizarImagenes" class="btn btn-primary mt-3">
		        </div>
		    </form>
		</div>
	</div>

	<script type="text/javascript">
		function clickInput(id) {
			document.getElementById("imagen"+id).click();
		}

		function validarFormulario(){
			var repetido=false;
			var selects= [];

			//Obtengo todos los selects
			for (var x=0;x<5;x++){
				selects[x]= document.getElementById("select"+x);
			}

			//Recorro cada select y comparo con los demas a ver si hay alguno que tenga el mismo orden repetido
			for(var i=0;i<4 && !repetido;i++){
				var estaSeleccionado = selects[i].options[selects[i].selectedIndex].value;
				for(var j=i+1;j<5;j++){
					var estaSeleccionadoAux = selects[j].options[selects[j].selectedIndex].value;
					if( estaSeleccionado == estaSeleccionadoAux ){
						repetido=true;
						document.getElementById("div-alert-selects").style.display = "block";
						break;
					}
				}
			}

			//Pongo la cantidad de checkboxs chequeados en 0, si encuentra alguno sale del for y no pasa nada
			var cant=0;
			for (var x=0;x<5;x++){
				if(document.getElementById("mostrar"+x).checked == true){
					cant=cant+1;
					break;
				}
			}

			//Si la cantidad de checkboxs chequeados sigue siendo 0, entonces freno el envio del formulario
			if(cant==0){
				repetido=true;
				document.getElementById("div-alert-checkboxs").style.display = "block";
			}

			if(repetido){
				event.preventDefault();
			}
		}
	</script>

	<script type="text/javascript">
	    function checkextension(id) {
	        var file = document.querySelector("#imagen"+id);
	        if ( /\.(jpg|png|jpeg)$/i.test(file.files[0].name) === false ) {
	            document.getElementById("imagen"+id).value="";
	            document.getElementById("div-alert-extension").style.display = "block";
	        }
	    }
	</script>

	<script type="text/javascript">
	    $('#imagen0').change(function(e) {
	        var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
	        if ( !allowedExtensions.exec($('#imagen0').val())) {
	            $('#imagen0').val('');
	            document.getElementById("div-alert").style.display = "block";
	        } else {
	            addImage(e,0); 
	        }
	     });

	    $('#imagen1').change(function(e) {
	    	var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
	        if ( !allowedExtensions.exec($('#imagen1').val())) {
	            $('#imagen1').val('');
	            document.getElementById("div-alert").style.display = "block";
	        } else {
	            addImage(e,1); 
	        }
	     });
	    
	    $('#imagen2').change(function(e) {
	    	var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
	        if ( !allowedExtensions.exec($('#imagen2').val())) {
	            $('#imagen2').val('');
	            document.getElementById("div-alert").style.display = "block";
	        } else {
	            addImage(e,2); 
	        }
	     });
	    
	    $('#imagen3').change(function(e) {
	    	var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
	        if ( !allowedExtensions.exec($('#imagen3').val())) {
	            $('#imagen3').val('');
	            document.getElementById("div-alert").style.display = "block";
	        } else {
	            addImage(e,3); 
	        }
	     });

	    $('#imagen4').change(function(e) {
			var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
		    if ( !allowedExtensions.exec($('#imagen4').val())) {
		        $('#imagen4').val('');
		        document.getElementById("div-alert").style.display = "block";
		    } else {
		        addImage(e,4); 
		    }
		 });

		function addImage(e,id){
			//Obtengo la imagen que se cargo en el input
		    var file = e.target.files[0],
		    imageType = /image.*/;
		    
		    if (!file.type.match(imageType)) return;

		    //Guardo en imagen el elemento <img> relacionado al input modificado
		    var imagen = document.getElementById("imgSalida"+id);
		    imagen.title = file;

		    var reader = new FileReader();

		    //Le seteo el atributo src a la imagen, con la imagen que se cargo en el input
		    reader.onload = function(event,id) {
			    imagen.src = event.target.result;
			  };
		    reader.readAsDataURL(file);
		}
	</script>
</body>

<?php
	include_once("scripts.php");
?>