<?php
	require_once "../conexionDB.php";
	$conectar=new ConexionDB();
    $conexion=$conectar->inicializar();

	$instruccion = "select imagen,titulo_imagen,texto_imagen,orden from carrousel_app where mostrar=1 order by orden ASC";
	$consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

	$nfilas = mysqli_num_rows($consulta);

	$info_carrousel = array();
	$imagenes = array();
	$imagen = array();

	$info_carrousel['ruta'] = __DIR__."/imagenes/";

	for ($i=0; $i < $nfilas; $i++) { 
		$fila = mysqli_fetch_array($consulta);

		$imagen['imagen'] = $fila['imagen'];
		$imagen['titulo_imagen'] = $fila['titulo_imagen'];
		$imagen['texto_imagen'] = $fila['texto_imagen'];
  		
  		array_push($imagenes, $imagen);
	}

	$info_carrousel['items_carrousel'] = $imagenes;
	echo json_encode($info_carrousel);  	
?>