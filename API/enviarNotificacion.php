<?php
    require_once "../conexionDB.php";
    $conectar = new ConexionDB();
    $conexion = $conectar->inicializar();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAFhT0p3c:APA91bGBZp1JfNHiExuFwvIYW3ap0Qg7T5he4jpjszaBBWVv25bbu2dvJMBrMhG4KLGqvn6-YrpNpkZnNGgUikw6EO5IngzSi5SlFS8eMmR-v5UyRnNHSvE-s9yHPipfkfntedV0aWkL';
        
        $id_usuario = $_POST['id_cliente'];
        $title = $_POST['titulo'];
        $body = $_POST['cuerpo'];

        $sql="SELECT tokens.token FROM tokens,tokens_x_usuarios where  tokens_x_usuarios.id_usuario = ".$id_usuario." and tokens_x_usuarios.id_token = tokens.id_token;";
        $consulta=$conexion->query($sql);
        $nfilas = mysqli_num_rows($consulta);

        for($i=0;$i<$nfilas;$i++){
            $fila = mysqli_fetch_array($consulta);
            $token = $fila['token'];

            $notification = array('title' => $title , 'body' => $body, 'sound' => 'default', 'badge' => '1');
            $data = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK','title' => $title , 'body' => $body);
            $arrayToSend = array('to' => $token , 'notification' => $notification , 'priority' => 'high' , 'data' => $data);    
            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key='. $serverKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            //Para que no imprima el response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            //Send the request
            $response = curl_exec($ch);
            //Close request
            if ($response === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
        }

        $sql = "SELECT id_usuario FROM usuarios_panel LIMIT 1;";
        $consulta = $conexion->query($sql);
        $fila = mysqli_fetch_array($consulta);
        $id_usuario_panel = $fila['id_usuario'];

        $sql1 = "INSERT INTO notificaciones_app (titulo,cuerpo,id_creador,fecha_envio,personalizada) VALUES('".$title."','".$body."',".$id_usuario_panel.",'".date("Y-m-d G:i:s")."',1);";
        $consulta1 = $conexion->query($sql1);
        $id_notificacion = mysqli_insert_id($conexion); 

        $sql2 = "INSERT INTO notificaciones_x_usuarios (id_notificacion,id_usuario) VALUES(".$id_notificacion.",".$id_usuario.");";
        $consulta2 = $conexion->query($sql2);
    }
?>
