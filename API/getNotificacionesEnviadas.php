<?php 
	require_once "../conexionDB.php";
	$conectar=new ConexionDB();
    $conexion=$conectar->inicializar();
	
	$id_usuario = $_POST['id_usuario'];

    $instruccion = "select notificaciones_app.id_notificacion,notificaciones_app.titulo,notificaciones_app.cuerpo from notificaciones_app, notificaciones_x_usuarios, usuarios_locucionar where usuarios_locucionar.id_usuario = $id_usuario and notificaciones_x_usuarios.id_usuario = $id_usuario and notificaciones_app.id_notificacion = notificaciones_x_usuarios.id_notificacion order by notificaciones_app.id_notificacion desc LIMIT 5";
	$consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar las notificaciones del usuario ".$id_usuario);

	$nfilas = mysqli_num_rows($consulta);

	$notificaciones = array();
	$notificaciones['error'] = "";

	$notificaciones_aux = array();
	$notificacion = array();
	for ($i=0; $i < $nfilas; $i++) { 
		$fila = mysqli_fetch_array($consulta);
		$notificacion['id_notificacion'] = $fila['id_notificacion'];
		$notificacion['titulo'] = $fila['titulo'];
		$notificacion['cuerpo'] = $fila['cuerpo'];
  		array_push($notificaciones_aux, $notificacion);
	}

	$notificaciones['notificaciones'] = $notificaciones_aux;

	echo json_encode($notificaciones);
?>