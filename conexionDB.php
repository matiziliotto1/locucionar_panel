<?php
    class ConexionDB{
        private $conexion;

        public function __construct()
        {
            $this->conexion=new mysqli("localhost","julian","julian","locucionar");
            if($this->conexion->connect_error){
                die("Error de conexion: ". $this->conexion->connect_error);
            }
        }
        public function inicializar(){
            return $this->conexion;
        }
        //Cuando no se usa el objeto se cierra la conexion
        function __destruct(){
            $this->conexion->close();
        }
    }
?>