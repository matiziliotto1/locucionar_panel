<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index - LocucionAr App</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
    <!-- https://fonts.google.com/specimen/Roboto -->
    <link rel="stylesheet" href="css/fontawesome.min.css">
    <!-- https://fontawesome.com/ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- https://getbootstrap.com/ -->
    <link rel="stylesheet" href="css/templatemo-style.css">
    <?php
    function active($pag)
    {
        $url_array =  explode('/', $_SERVER['REQUEST_URI']);
        //Me quedo con el ultimo 'string' que es nombreArchivo.php
        $url = end($url_array);
        if ($pag == $url) {
            echo 'active'; //class name in css 
        }
    }
    ?>
</head>

<body id="reportsPage">
    <div class="" id="home">
        <nav class="navbar navbar-expand-xl">
            <div class="container h-100">
                <a class="navbar-brand" href="index.php">
                    <h1 class="tm-site-title mb-0">LocucionAr</h1>
                </a>
                <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars tm-nav-icon"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto h-100">
                        <li class="nav-item">
                            <a class="nav-link <?php active('index.php') ?> <?php active('') ?>" href="index.php">
                                <i class="far fa-file-alt"></i>
                                <span>
                                    Notificaciones
                                </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link <?php active('configCarrousel.php') ?>" href="configCarrousel.php">
                                <i class="fas fa-images"></i>
                                Carrusel
                            </a>
                        </li>

                        <?php
                            if(isset($_SESSION['usuarioAdmin']) && $_SESSION['usuarioAdmin']==1 ){
                                echo    '<li class="nav-item">
                                            <a class="nav-link '; active('usuarios.php');echo '" href="usuarios.php">
                                                <i class="far fa-user"></i>
                                                Usuarios
                                            </a>
                                        </li>';
                            }
                        ?>

                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="login.php?logout=true">
                                <i class="fas fa-lock"></i>
                                <b>Logout</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>