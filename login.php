<?php
session_start();
require_once "controladorUsuarios.php";
?>

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Ingresa - LocucionAr App</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700" />
  <!-- https://fonts.google.com/specimen/Open+Sans -->
  <link rel="stylesheet" href="css/fontawesome.min.css" />
  <!-- https://fontawesome.com/ -->
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <!-- https://getbootstrap.com/ -->
  <link rel="stylesheet" href="css/templatemo-style.css">


  <?php
  $error = false;
  if (isset($_REQUEST['logout'])) {
    $controladorUsuario = new ControladorUsuarios();
    $controladorUsuario->logout();
  }
  //SI YA INICIASTE SESION TE MANDA PARA EL INDEX
  if (!isset($_SESSION['usuarioValido'])) {
    if (isset($_REQUEST['username']) and isset($_REQUEST['password'])) {
      $controladorUsuario = new ControladorUsuarios();
      $estaAutenticado = $controladorUsuario->autenticar($_REQUEST['username'], $_REQUEST['password']);
      if ($estaAutenticado) {
        $_SESSION['usuarioValido'] = $_REQUEST['username'];
        $_SESSION['usuarioAdmin'] = $controladorUsuario->isAdmin($_REQUEST['username']);
        header("Location: index.php");
      } else {
        $error = true;
      }
    }
  } else {
    header("Location: index.php");
  }
  ?>

</head>

<body>
  <div class="container tm-mt-big tm-mb-big">
    <div class="row">
      <div class="col-12 mx-auto tm-login-col">
        <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
          <div class="row">
            <div class="col-12 text-center">
              <h2 class="tm-block-title mb-4">Bienvenido al panel de la App. Ingresa</h2>
              <?php if ($error) { ?>
                <div class="alert alert-danger" role="alert">
                  Usuario o contraseña incorrectos!
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-12">
              <form action="" method="post" class="tm-login-form">
                <div class="form-group">
                  <label for="username">Usuario</label>
                  <input name="username" type="text" class="form-control validate" id="username" value="" required />
                </div>
                <div class="form-group mt-3">
                  <label for="password">Contraseña</label>
                  <input name="password" type="password" class="form-control validate" id="password" value="" required />
                </div>
                <div class="form-group mt-4">
                  <button type="submit" class="btn btn-primary btn-block text-uppercase">
                    Ingresa
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="js/jquery-3.3.1.min.js"></script>
  <!-- https://jquery.com/download/ -->
  <script src="js/bootstrap.min.js"></script>
  <!-- https://getbootstrap.com/ -->
</body>