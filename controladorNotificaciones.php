<?php
    require_once "conexionDB.php";

    class controladorNotificaciones{
        
        //SOLO ACEPTA VALORES ALFANUMERICOS, NO ACEPTA MAILS
        function sanitizar($frase){
            $paso=false;
            $expresion="/^[a-zA-ZñÑ0-9]+$/";
            $paso=preg_match($expresion,$frase);
            return $paso;
        }
        
        function crearNotificacion(array $request){
            //TENGO QUE SANITIZAR ACA EL TEXTO Y EL CUERPO
            //if(self::sanitizar($request['tituloNotificacion']) and self::sanitizar($request['cuerpoNotificacion'])){
                $error=array();
                $conectar=new ConexionDB();
                $conexion=$conectar->inicializar();
                $idUsuario=0;
                try {
                    $userName=$_SESSION['usuarioValido'];
                    $sql="SELECT `id_usuario` FROM `usuarios_panel` WHERE usuario='$userName'";
                    $consulta=$conexion->query($sql);
                    if(! $consulta){
                        throw new Exception("Error creando notificacion", 1);
                    }
                    $row = mysqli_fetch_assoc($consulta);
                    $idUsuario=$row['id_usuario'];
                    $error['valor']=true;
                    $error['mensaje']='Notificacion creada correctamente';
                } catch (Exception $error) {
                    $error['false']=true;
                    $error['mensaje']='Error eliminando notificacion';
                }

                if($idUsuario != 0){
                    try {
                        $sql="INSERT INTO `notificaciones_app`(`id_notificacion`, `titulo`, `cuerpo`, `id_creador`, `fecha_envio`) VALUES (NULL,'$request[tituloNotificacion]','$request[cuerpoNotificacion]',$idUsuario,NULL)";
                        $consulta=$conexion->query($sql);
                        if(! $consulta){
                            throw new Exception("Error creando notificacion", 1);
                        }
                        $error['valor']=true;
                        $error['mensaje']='Notificacion creada correctamente';
                    } catch (Exception $e) {
                        $error['valor']=false;
                        $error['mensaje']='Error eliminando notificacion';
                    }
                }
                return $error;
            //}
        }

        function getNotificaciones(){
            $notificaciones=array();
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();
            $sql="SELECT * FROM notificaciones_app WHERE personalizada=0  order by id_notificacion desc";
            $consulta=$conexion->query($sql) or die ("Error al listar las notificaciones");
            while($row = mysqli_fetch_array($consulta)) {
                $notificaciones[]=$row;
            }
            return $notificaciones;
        }

        function getNotificacion($id=""){
            $notificaciones=array();
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();
            $sql="SELECT * FROM notificaciones_app WHERE id_notificacion=$id";
            $consulta=$conexion->query($sql) or die ("Error al buscar la notificacion");
            while($row = mysqli_fetch_assoc($consulta)) {
                $notificaciones[]=$row;
            }
            return $notificaciones[0];
        }

        function eliminarNotificacion(array $request){
            $error=array();
            try {
                $conectar=new ConexionDB();
                $conexion=$conectar->inicializar();
                $sql2="DELETE FROM notificaciones_x_usuarios WHERE id_notificacion=$request[id]";
                $consulta=$conexion->query($sql2);
                $sql="DELETE FROM notificaciones_app WHERE id_notificacion=$request[id]";
                $consulta=$conexion->query($sql);
                if(! $consulta){
                    throw new Exception("Error eliminando notificacion", 1);
                }
                $error['valor']=true;
                $error['mensaje']='Notificacion eliminada correctamente';
            } catch (Exception $e) {
                $error['valor']=false;
                $error['mensaje']='Error eliminando notificacion';
            }
            return $error;
        }

        function enviarNotificacion(array $request){
            $error=array();
            try{
                $conectar = new ConexionDB();
                $conexion = $conectar->inicializar();
                $sql = "SELECT * FROM notificaciones_app WHERE id_notificacion=".$request['id'].";";
                $consulta = $conexion->query($sql);
                $file = mysqli_fetch_array($consulta);
                $title = $file['titulo'];
                $body = $file['cuerpo'];

                $url = "https://fcm.googleapis.com/fcm/send";
                $serverKey = 'AAAAFhT0p3c:APA91bGBZp1JfNHiExuFwvIYW3ap0Qg7T5he4jpjszaBBWVv25bbu2dvJMBrMhG4KLGqvn6-YrpNpkZnNGgUikw6EO5IngzSi5SlFS8eMmR-v5UyRnNHSvE-s9yHPipfkfntedV0aWkL';
                
                $sql1="SELECT tokens.token,usuarios_locucionar.id_usuario FROM usuarios_locucionar,tokens,tokens_x_usuarios where  usuarios_locucionar.id_usuario = tokens_x_usuarios.id_usuario and tokens_x_usuarios.id_token = tokens.id_token;";
                $consulta1=$conexion->query($sql1);
                $nfilas = mysqli_num_rows($consulta1);

                for($i=0;$i<$nfilas;$i++){
                    $fila = mysqli_fetch_array($consulta1);
                    $token = $fila['token'];

                    $notification = array('title' => $title , 'body' => $body, 'sound' => 'default', 'badge' => '1');
                    $data = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK','title' => $title , 'body' => $body);
                    $arrayToSend = array('to' => $token , 'notification' => $notification , 'priority' => 'high' , 'data' => $data);    
                    $json = json_encode($arrayToSend);
                    $headers = array();
                    $headers[] = 'Content-Type: application/json';
                    $headers[] = 'Authorization: key='. $serverKey;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                    curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                    //Para que no imprima el response
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    //Send the request
                    $response = curl_exec($ch);
                    //Close request
                    if ($response === FALSE) {
                        die('FCM Send Error: ' . curl_error($ch));
                    }
                    else{
                        $sql = "INSERT INTO notificaciones_x_usuarios (id_notificacion,id_usuario) VALUES(".$request['id'].",".$fila['id_usuario'].");";
                        $consulta=$conexion->query($sql);
                    }
                    curl_close($ch);
                }

                $sql = "UPDATE notificaciones_app SET fecha_envio = '".date("Y-m-d G:i:s")."' WHERE notificaciones_app.id_notificacion = ".$request['id'].";";
                $consulta=$conexion->query($sql);
                
                $error['valor']=true;
                $error['mensaje']='Notificacion enviada correctamente';    
            }
            catch (Exception $e) {
                $error['valor']=false;
                $error['mensaje']='Error enviando notificacion';
            }
            return $error;
        }
    }
?>