<?php
session_start();
if (!isset($_SESSION['usuarioValido'])) {
    header("Location: login.php");
}
require_once('controladorNotificaciones.php');
include_once('header.php');
?>

<?php
$controladorNotificaciones = new controladorNotificaciones();
$error = array();
if (isset($_REQUEST['opcion'])) {
    switch ($_REQUEST['opcion']) {
        case 'nuevaNotificacion':
            $error=$controladorNotificaciones->crearNotificacion($_REQUEST);
            break;
        case 'eliminarNotificacion':
            $error=$controladorNotificaciones->eliminarNotificacion($_REQUEST);
            break;
        case 'enviarNotificacion':
            $controladorNotificaciones->enviarNotificacion($_REQUEST);
            break;
    }
}
$notificaciones = $controladorNotificaciones->getNotificaciones();
?>


<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col d-inline">
        <?php if (isset($error['valor'])) {
            if ($error['valor']) { ?>

                <div class="alert alert-success" role="alert">
                    <?php echo ($error['mensaje']); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php    } else { ?>

                <div class="alert alert-danger" role="alert">
                    <?php echo ($error['mensaje']); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        <?php }
        } ?>
        <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-overflow">
            <h2 class="tm-block-title">Nueva notificacion: </h2>
            <div id="formularioNotificacion">
                <form method="post" action="index.php?opcion=nuevaNotificacion">
                    <div class="form-group">
                        <label for="tituloNotificacion">Titulo: </label>
                        <input type="text" maxlength="60" name="tituloNotificacion" id="tituloNotificacion" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="textoNotificacion">Texto notificacion: </label>
                        <textarea class="form-control" name="cuerpoNotificacion" id="textoNotificacion" rows="3"></textarea>
                    </div>
                    <div style="margin-top: 30px; margin-bottom:10px">
                        <button type="submit" class="btn btn-primary">Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col d-inline">
        <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-overflow">
            <h2 class="tm-block-title">Notificaciones: </h2>
            <div class="tm-notification-items">
                <?php foreach ($notificaciones as $notificacion) { ?>
                    <div class="media tm-notification-item">
                        <div class="media-body">
                            <i class="fas fa-bell fa-6x pr-3" style="color:#f5a623;position:relative;float:right"></i>
                            <p class="mb-2"><b>Titulo:</b>
                                <?php echo ($notificacion['titulo']); ?>
                            </p>
                            <p class="mb-2"><b>Texto:</b>
                                <?php echo ($notificacion['cuerpo']); ?>
                            </p>
                            <?php if (is_null($notificacion['fecha_envio'])) {
                                echo ('<p class="mb-2"><b>Enviada: </b> No </p>');
                                echo ('<p class="mb-2"><b>Fecha envio: </b> </p> <br>');

                                echo ('<a href=index.php?opcion=enviarNotificacion&id=' . $notificacion["id_notificacion"] . ' class="btn btn-secondary btn-sm">Enviar</a>');
                            } else {
                                echo ('<p class="mb-2"><b>Enviada: </b> Si </p>');
                                echo ('<p class="mb-2"><b>Fecha envio: </b>' . $notificacion['fecha_envio'] . ' </p> <br>');
                                echo ('<a href=index.php?opcion=enviarNotificacion&id=' . $notificacion["id_notificacion"] . ' class="btn btn-secondary btn-sm">Reenviar</a>');
                            } ?>
                            <button type="button" data-toggle="modal" data-target="#modalEliminarNotificacion" value="<?php echo ($notificacion['id_notificacion']) ?>" class="btn btn-danger btn-sm">Eliminar</button>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</div>
</body>

<!--LOS MODAL VAN FUERA DEL BODY -->
<div class="modal fade" id="modalEliminarNotificacion" tabindex="-1" role="dialog" aria-labelledby="modalEliminarNotificacion" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:whitesmoke">Eliminar notificacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title" style="color:whitesmoke">¿Desea eliminar la notificacion?</h3><br>
                <p style="color:whitesmoke" id="parrafoEliminarNotificacion"></p><br>
                <form action="" method="get">
                    <div id="botonEliminarNotificacion">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .modal-content {
        background-color: #567086 !important;
    }
</style>


<?php
include_once("scripts.php");
?>