<?php
session_start();
if (!isset($_SESSION['usuarioValido'])) {
    header("Location: login.php");
}
if (isset($_SESSION['usuarioAdmin']) && $_SESSION['usuarioAdmin'] != 1) {
    header("Location: index.php");
}
include_once('header.php');
require_once('controladorUsuarios.php');
?>

<?php
$controladorUsuarios = new ControladorUsuarios();
$error = array();

if (isset($_REQUEST['opcion'])) {
    switch ($_REQUEST['opcion']) {
        case 'modificarUser':
            $error = $controladorUsuarios->modificarUser($_REQUEST);
            break;
        case 'agregarUser':
            $error = $controladorUsuarios->agregarUsuario($_REQUEST);
            break;
        case 'eliminarUser':
            $error = $controladorUsuarios->eliminarUsuario($_REQUEST);
            break;
    }
}
$usuarios = $controladorUsuarios->getUsuarios();
?>


<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col pt-4">
        <?php if (isset($error['valor'])) {
            if ($error['valor']){ ?>

                <div class="alert alert-success" role="alert">
                    <?php echo ($error['mensaje']); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php    } else { ?>
                
                <div class="alert alert-danger" role="alert">
                    <?php echo ($error['mensaje']); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        <?php }
        } ?>
        <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-overflow">
            <h2 class="tm-block-title">Usuarios: </h2>
            <div class="tm-notification-items">
                <?php foreach ($usuarios as $usuario) { ?>
                    <div class="media tm-notification-item">
                        <div class="tm-gray-circle"><img src="img/user2.png" alt="Avatar Image" class="rounded-circle"></div>
                        <div class="media-body">
                            <p class="mb-2"><b>Nombre: <?php echo ("$usuario[usuario]"); ?></b> <br></p>
                            <p class="mb-2"><b>Administrador: <?php $usuario['administrador'] == 1 ? print("si") : print("no"); ?></b></p> <br>
                            <button type="button" class="btn btn-light btn-sm" data-toggle="modal" id="modificarUsuarioButton" data-target="#modalFormulario" value=<?php echo ($usuario['id_usuario']) ?> onclick="eliminarError();">Modificar</button>
                            <?php if($usuario['usuario']!=$_SESSION['usuarioValido']){ ?>
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" id="eliminarUsuarioButton" data-target="#modalEliminar" value=<?php echo ($usuario['id_usuario']) ?>>Eliminar</button>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div style="margin-top: 2%; margin-bottom:2%">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFormulario" id="agregarUsuarioButton" value="Agregar" onclick="eliminarError();">Agregar</button>
            </div>
        </div>
    </div>

</div>
</body>

<!--LOS MODAL VAN FUERA DEL BODY -->
<div class="modal fade" id="modalFormulario" tabindex="-1" role="dialog" aria-labelledby="modalFormulario" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle" style="color:whitesmoke">Formulario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formularioModal" method="post" action="" autocomplete="off">
                    <div class="form-group">
                        <label for="nombreModal">Nombre: </label>
                        <input type="text" name="nombre" id="nombreModal" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="apellidoModal">Apellido: </label>
                        <input type="text" name="apellido" id="apellidoModal" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="userNameModal">Usuario: </label>
                        <input type="text" name="userName" id="userNameModal" class="form-control" required>
                    </div>

                    <div class="form-group" id="divPasswordModal">
                        <label for="passwordModal">Contraseña: </label>
                        <input type="password" name="password" id="passwordModal" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="administradorModal">Administrador: </label>
                        <input type="checkbox" name="administrador" id="administradorModal" style="margin-left:10px">
                    </div>

                    <div id="botonModal" style="margin-top: 30px; margin-bottom:10px">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="modalEliminar" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle" style="color:whitesmoke">Eliminar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if (isset($error['value'])) {
                    if ($error['value']) { ?>

                        <div class="alert alert-danger" role="alert">
                            <?php echo ($error['mensaje']); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                <?php    }
                } ?>
                <h3 class="modal-title" style="color:whitesmoke">¿Desea eliminar el usuario?</h3><br>
                <p style="color:whitesmoke" id="parrafoEliminar"> </p><br>
                <form action="" method="get">
                    <div id="botonEliminar">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .modal-content {
        background-color: #567086 !important;
    }
</style>


<?php
include_once("scripts.php");
?>